﻿using System;

namespace WebAPIBase.Model
{
    public class User
    {
        public int Id { get; set; }
        public string email { get; set; }
        public string Password { get; set; }
        public string Active { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string EditedBy { get; set; }
        public string EditedDate { get; set; }
    }
}