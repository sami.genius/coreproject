﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebAPIBase.Model
{
    public class Application
    {
        public int Id { get; set; }
        public int ApplicationId { get; set; }
        public string LocalUserName { get; set; }
        public int LocalUserId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string EditedBy { get; set; }
        public string EditedDate { get; set; }

    }
}
