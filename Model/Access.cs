﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebAPIBase.Model
{
   public class Access
    {
        public int Id { get; set; }
        public string ConnString { get; set; }
        public string Info { get; set; }
        public string UserTableName { get; set; }
    }
}
