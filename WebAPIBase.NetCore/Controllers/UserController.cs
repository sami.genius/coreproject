﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPIBase.API.Requests;
using WebAPIBase.Service.Interface;

namespace WebAPIBase.API.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        protected readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Get")]
        public ActionResult ValidateUser([FromBody]UserLogin request)
        {
            var ret = _userService.GetToken(request.Email, request.Password);

            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("GetAll")]
        public JsonResult GetAllUser()
        {
            var getUserIfo = _userService.GetUserList();
            return Json(getUserIfo);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("create")]
        public JsonResult InsertUser(string username, string password, string confirmpassword)
        {
            if (password == confirmpassword)
            {
                var Active = true;
                _userService.InsertUser(username, password, Active);
            }
            return Json("User Created Successfully! :)");
        }

        [AllowAnonymous]
        [HttpPut]
        [Route("Update")]
        public JsonResult UpdateUser(int Id, string email, string password, string confirmpassword)
        {
            if (password == confirmpassword)
            {
              
                _userService.UpdateUser(Id, email, password);
            }
            return Json("User Updated Successfully! :)");
        }

        [AllowAnonymous]
        [HttpDelete]
        [Route("Delete")]
        public JsonResult DeleteUser(int Id)
        {
            var Active = 0;
            _userService.DeleteUser(Id, Active);

            return Json("User Deleted Successfully! :)");
        }
    }
}