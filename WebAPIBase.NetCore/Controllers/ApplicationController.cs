﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPIBase.API.Requests;
using WebAPIBase.Service.Interface;

namespace WebAPIBase.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Application")]
    public class ApplicationController : Controller
    {
        protected readonly IApplicationService _applicationService;

        public ApplicationController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("Get")]
        public ActionResult ValidateUser([FromBody] ApplicationLogin request)
        {
            var application = _applicationService.GetToken(request.Id);
            return Json(application);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("GetAll")]
        public JsonResult GetAllApplication()
        {
            var getapplicationInfo = _applicationService.GetApplicationList();
            return Json(getapplicationInfo);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("create")]
        public JsonResult InsertApplication(int UserId, int ApplicationId, string LocalUserName, int LocalUserId)
        {
            var Active = true;
            _applicationService.InsertApplication(UserId, ApplicationId, LocalUserName, LocalUserId, Active);
            return Json("Application Created Successfully ! :)");
        }
        [AllowAnonymous]
        [HttpPut]
        [Route("create")]
        public JsonResult UpdateApplication(int Id,int UserId, int ApplicationId, string LocalUserName, int LocalUserId)
        {
            var Active = true;
            _applicationService.UpdateApplication(Id,UserId, ApplicationId, LocalUserName, LocalUserId, Active);
            return Json("Application Created Successfully ! :)");
        }
        public JsonResult DeleteApplication(int Id)
        {
            var Active = false;
            _applicationService.DeleteApplication(Id, Active);
            return Json("Application has been Deleted :");
        }
    }
}
