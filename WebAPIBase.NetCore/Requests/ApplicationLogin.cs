﻿using Newtonsoft.Json;

namespace WebAPIBase.API.Requests
{
    public class ApplicationLogin
    {
        [JsonProperty(PropertyName = "Id", NullValueHandling = NullValueHandling.Ignore)]
        public int Id { get; set; }
    }
}
