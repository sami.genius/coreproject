﻿using Newtonsoft.Json;

namespace WebAPIBase.API.Requests
{
    public class UserLogin
    {
        [JsonProperty(PropertyName = "email", NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "password", NullValueHandling = NullValueHandling.Ignore)]
        public string Password { get; set; }
       
    }
}