﻿using System;
using System.Collections.Generic;
using System.Text;
using WebAPIBase.Model;

namespace WebAPIBase.Data.DapperORM.Interface
{
   public interface IApplicationRepository
    {
        ApplicationException ValidateApplication();
        List<Application> GetApplicationList();
        void InsertApplication(int UserId, int ApplicationId, string LocalUserName, int LocalUserId, bool active);
        Application UpdateApplication(int Id,int UserId, int ApplicationId, string LocalUserName, int LocalUserId);
        Application DeleteApplication();
    }
}
