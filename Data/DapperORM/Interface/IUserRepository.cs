﻿using System.Collections.Generic;
using WebAPIBase.Model;

namespace WebAPIBase.Data.DapperORM.Interface
{
    public interface IUserRepository
    {
        User ValidateUser(string username, string password);
        List<User> GetUserList();

        void InsertUser(string Email, string Password, bool Active);
        User UpdateUser(int id, string username, string password);
        User DeleteUser(int id, int active);
    }
}