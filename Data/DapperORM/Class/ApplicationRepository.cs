﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WebAPIBase.Data.DapperORM.Interface;
using WebAPIBase.Model;

namespace WebAPIBase.Data.DapperORM.Class
{
    public class ApplicationRepository : BaseRepository, IApplicationRepository
    {
        public Application DeleteApplication()
        {
            throw new NotImplementedException();
        }

        public List<Application> GetApplicationList()
        {
            using var db = GetMySqlConnection();
            const string sql = @"Select * from Application";
            return db.Query<Application>(sql).ToList();
        }

        public void InsertApplication(int UserId, int ApplicationId, string LocalUserName, int LocalUserId, bool Active)
        {
            using var db = GetMySqlConnection();
            const string sql = @"insert into Application (UserId, ApplicationId, LocalUserName, LocalUserId, active, CreatedDate) values (@UserId, @ApplicationId, @LocalUserName, @LocalUserId, @Active, Now())";
            db.Execute(sql, new { UserId = UserId, ApplicationId = ApplicationId, LocalUserName = LocalUserName, Active = Active }, commandType: CommandType.Text);
        }

        public Application UpdateApplication(int Id, int UserId, int ApplicationId, string LocalUserName, int LocalUserId)
        {
            using var db = GetMySqlConnection();
            const string sql = @"Update application Set UserId=@UserId, ApplicationId=@ApplicationId,LocaUserName = @LocalUserName LocalUserId=@LocalUserId, EditedDate = Now() where Id=@Id";
            return db.Query<Application>(sql, new { Id = Id, UserId = UserId, ApplicationId = ApplicationId, LocalUserName = LocalUserName, LocalUserId = LocalUserId }, commandType: CommandType.Text).FirstOrDefault();
        }

        public ApplicationException ValidateApplication()
        {
            throw new NotImplementedException();
        }
    }
}
