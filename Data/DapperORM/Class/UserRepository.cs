﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WebAPIBase.Data.DapperORM.Interface;
using WebAPIBase.Model;

namespace WebAPIBase.Data.DapperORM.Class
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public User ValidateUser(string username, string password)
        {
            using var db = GetMySqlConnection();
            const string sql = @"Select Id, Email, Active from User where Email = @Email and Password = @Password";

            return db.Query<User>(sql, new { Email = username, Password = password }, commandType: CommandType.Text).FirstOrDefault();
        }
        public List<User> GetUserList()
        {
            using var db = GetMySqlConnection();
              const string sql = @"Select * from User";
              return db.Query<User>(sql).ToList();
        }
        //public List<User> GetAll()
        //{
        //    using var db = GetMySqlConnection();
        //    const string sql = @"Select * from User";
        //    return db.Query<User>(sql).ToList();
        //}
        //public User GetAll()
        //{
        //    //using var db = GetMySqlConnection();
        //    //const string sql = @"Select Id, Email, Active from User";
        //    //return db.Query<User>(sql).ToList();
        //}
        public User UpdateUser(int id, string email, string password)
        {
            using var db = GetMySqlConnection();
            const string sql = @"Update user Set  Email=@Email, Password = @Password, EditedDate = Now() where Id=@Id";
            return db.Query<User>(sql, new { Email = email, Password = password, Id= id }, commandType: CommandType.Text).FirstOrDefault();
    
        }
        public User DeleteUser(int id, int active)
        {
            using var db = GetMySqlConnection();
            const string sql = @"Update user Set Active = @Active where Id =@Id";
            return db.Query<User>(sql, new { Id = id, Active = active }, commandType: CommandType.Text).FirstOrDefault();
        }
        public void InsertUser(string username, string password, bool isActive)
        {
            using var db = GetMySqlConnection();
            const string sql = @"insert into User (Email, Password, Active, CreatedDate) values (@Email, @Password, @Active, Now())";
            //const string sql = null;
            //db.Execute(sql, new { Login = username, Password = password}, commandType: CommandType.Text);
            db.Execute(sql, new { Email = username, Password = password, Active = isActive}, commandType: CommandType.Text);
        }
    }
}