﻿using System.Collections.Generic;
using WebAPIBase.Model;

namespace WebAPIBase.Service.Interface
{
    public interface IUserService
    {
        List<User> GetUserList();
        
        User GetToken(string email, string password);

        void InsertUser(string username, string password, bool Active);
        void UpdateUser(int id, string email, string password);
        void DeleteUser(int id, int active);
     
    }
}