﻿using System;
using System.Collections.Generic;
using System.Text;
using WebAPIBase.Model;

namespace WebAPIBase.Service.Interface
{
   public interface IApplicationService
    {
        List<Application> GetApplicationList();
        Application GetToken(int id);
        void InsertApplication(int UserId, int ApplicationId, string LocalUserName, int LocalUserId, bool active);
        void UpdateApplication(int Id,int UserId, int ApplicationId, string LocalUserName, int LocalUserId, bool active);
        void DeleteApplication(int Id, bool Active);
    }
}
