﻿using System.Collections.Generic;
using WebAPIBase.Data.DapperORM.Interface;
using WebAPIBase.Model;
using WebAPIBase.Service.Interface;

namespace WebAPIBase.Service.Class
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<User> GetUserList()
        {
            var user = _userRepository.GetUserList();
            //var obj = new List<User>();
               return user;
        }

        public User GetToken(string username, string password)
        {
            var passwordHash = Utils.HashUtil.GetSha256FromString(password);

            var ret = _userRepository.ValidateUser(username, passwordHash);

            if (ret != null)
            {
                //ret.Token = Utils.JwtManager.GenerateToken(username).Value;
            }
            return ret;
        }

        public void InsertUser(string username, string password, bool isActive)
        {
            var passwordHash = Utils.HashUtil.GetSha256FromString(password);

            _userRepository.InsertUser(username, passwordHash, isActive);
        }

        //public List<User> GetAll()
        //{
        //    //   throw new System.NotImplementedException();
        //    var user = _userRepository.GetAll();
        //    var obj = new List<User>();
        //    return obj;
        //}

        public void UpdateUser(int id, string username, string password)
        {
            var passwordHash = Utils.HashUtil.GetSha256FromString(password);
            _userRepository.UpdateUser(id, username, password);
        }


        public void DeleteUser(int id, int active)
        {
            _userRepository.DeleteUser(id, active);
        }

        //public IEnumerable<User> GetAll()
        //{
        //    var user = _userRepository.GetAll();
        //    var obj = new List<User>();
        //    return obj;
        //}

        //public User GetAll()
        //{
        //    var user = _userRepository.GetAll().to;
        //    var obj = new List<User>();
        //    return obj;
        //}



        //public void GetAll()
        //{
        //    var user = _userRepository.GetAll();
        //    return user;
        //}
    }
}