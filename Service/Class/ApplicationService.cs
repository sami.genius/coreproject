﻿using System.Collections.Generic;
using WebAPIBase.Data.DapperORM.Interface;
using WebAPIBase.Model;
using WebAPIBase.Service.Interface;

namespace WebAPIBase.Service.Class
{
   public  class ApplicationService : IApplicationService
    {
        private readonly IApplicationRepository _applicationRepository;
        public ApplicationService(IApplicationRepository applicationRepository)
        {
            _applicationRepository = applicationRepository;
        }

        public void DeleteApplication(int id, bool Active)
        {
            _applicationRepository.DeleteApplication();
        }

        public List<Application> GetApplicationList()
        {
            var application = _applicationRepository.GetApplicationList();
            return application;
        }

        public Application GetToken(int id)
        {
            throw new System.NotImplementedException();
        }

        public void InsertApplication(int UserId, int ApplicationId, string LocalUserName, int LocalUserId, bool active)
        {
            _applicationRepository.InsertApplication(UserId, ApplicationId, LocalUserName, LocalUserId, active);
        }
        public void UpdateApplication(int Id, int UserId, int ApplicationId, string LocalUserName, int LocalUserId, bool active)
        {
            _applicationRepository.UpdateApplication(Id,UserId,ApplicationId,LocalUserName,LocalUserId,active);
        }
    }
}
